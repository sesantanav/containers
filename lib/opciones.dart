import 'package:flutter/material.dart';

// Widget con las opciones de la aplicación

class Opciones extends StatefulWidget {

  @override
  _OpcionState createState() => _OpcionState();
}

class _OpcionState extends State<Opciones> {
  List<String> opciones = ['Opción 1', 'Opcion 2','Opción 3'];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(),
      child: SizedBox(
        height: 25,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: opciones.length,
          itemBuilder: (context, index) => buildOpciones(index),
        ),
      ),
    );
  }
    Widget buildOpciones(int index) {
      return Row(
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                  opciones[index]
              )
          )
        ],
      );
    }
}