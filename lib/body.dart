import 'package:flutter/material.dart';
import 'package:aplicacion1/opciones.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(
            'Home de la aplicación'
          ),
        ),
        Opciones(),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: GridView.count(
              padding: const EdgeInsets.all(8),
              crossAxisCount: 2,
              crossAxisSpacing: 20,
              mainAxisSpacing: 20,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(5),
                  child: Text(
                    'Recuadro 1'
                  ),
                  color: Colors.grey
                ),
                Container(
                  padding: const EdgeInsets.all(5),
                    child: Text(
                        'Recuadro 2'
                    ),
                    color: Colors.grey
                ),
                Container(
                    padding: const EdgeInsets.all(5),
                    child: Text(
                        'Recuadro 3'
                    ),
                    color: Colors.grey
                ),
                Container(
                    padding: const EdgeInsets.all(5),
                    child: Text(
                        'Recuadro 4'
                    ),
                    color: Colors.grey
                ),
                Container(
                    padding: const EdgeInsets.all(5),
                    child: Text(
                        'Recuadro 5'
                    ),
                    color: Colors.grey
                ),
                Container(
                    padding: const EdgeInsets.all(5),
                    child: Text(
                        'Recuadro 6'
                    ),
                    color: Colors.grey
                ),
              ],
            )
          )
        )
      ],
    );
  }
}