import 'package:flutter/material.dart';
import 'package:aplicacion1/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Registro()
    );
  }
}

class Registro extends StatefulWidget {

  @override
  _RegistroState createState() => _RegistroState();
}

// Scaffold: implementa una de diseño (layout) visual básica
// appBar, BackgroundColor, body, botone...
class _RegistroState extends State<Registro> {
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Center (
          child: Text(
            'Bienvenido',
            style: TextStyle(fontSize: 25)
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Center(
                child: Container(
                  width: 200,
                  height: 150,
                  decoration: BoxDecoration(
                    color: Colors.blueGrey,
                    borderRadius: BorderRadius.circular(50)
                  ),
                )
              )
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Ingresar Correo',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Ingresar Contraseña',
                ),
              ),
            ),
            FlatButton(
                onPressed: (){},
                child: Text(
                  'Olvidaste tu Contaseña'
                )
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.circular(50.0)
              ),
              child:FlatButton(
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (_) => Home())
                  );
                },
                child: Text(
                  'Ingresar',
                  style: TextStyle(color: Colors.white, fontSize: 20)
                ),
              )
            ),
          ],
        )
      ),
    );
  }
}

