import 'package:flutter/material.dart';
import 'package:aplicacion1/body.dart';

class Home extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            'Home Aplicación',
            style: TextStyle(fontSize: 25.0),
          )
        ),
      ),
      body: Body(),
    );
  }
}