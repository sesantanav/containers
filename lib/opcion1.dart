import 'package:flutter/material.dart';
import 'package:aplicacion1/opciones.dart';

class Opcion1 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Center(
            child: Text(
              'Pantalla Opción 1',
              style: TextStyle(fontSize: 25.0),
            )
        ),
      ),
      body: Opciones(),
    );
  }
}
